console.log('hello')
const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
  ];
 //je crée une nouvelle fonction pour pouvoir l'ajouter dans ma fonction addTask 
  const randomTask = { title: 'Ranger le salon', isComplete: false}
//je crée  ma fonction addTask et ajoute randomTask avec tasks pour que addTask ajoute randomTask à sa liste
  const addTask = (tasks, randomTask) => {
    return [...tasks, randomTask];
  };
console.log(addTask);

// dans cette fonction filter va nous permettre d'afficher seulment les objets dans le tableau tasks qui ont isComplete: false 
const removeTask = tasks.filter(tasks => {return tasks.isComplete === false;

})

console.log(removeTask);

//dans cette fonction seul les objets dans tasks qui ont isComplete:true s'afficheront
const finishedTasks = tasks.filter(tasks => {return tasks.isComplete === true;

}) 
 console.log(finishedTasks)

//map nous permet de changer les objets dans task qui ont is complte: true en is complete false et inversement
const toggleTaskStatus = tasks.map(tasks => {
   if (tasks.isComplete === true){
    return {...tasks, isComplete: false }
} else {
    return {...tasks, isComplete: true}
}
});

console.log(toggleTaskStatus)